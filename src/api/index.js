import request from '@/utils/request'

const api = {
  user: {
    login(data) {
      return request({
        url: 'api/management/user/login',
        method: 'post',
        data
      })
    },
    logout(data) {
      return request({
        url: 'api/management/user/logout',
        method: 'post',
        // header: {
        //   'Authorization': 'bearer ' + bearerToken
        // },
        data
      })
    },
    info(data) {
      return request({
        url: 'api/management/user/info',
        method: 'get'
      })
    }
  },
  periodical: {
    list(params) {
      return request({
        url: 'api/management/periodical/list',
        method: 'get',
        params
      })
    },
    detail(params) {
      return request({
        url: 'api/management/periodical/detail',
        method: 'get',
        params
      })
    },
    getDirectionOptions() {
      return request({
        url: 'api/periodical/getDirectionOptions',
        method: 'get'
      })
    },
    getCycleOptions() {
      return request({
        url: 'api/periodical/getCycleOptions',
        method: 'get'
      })
    },
    getAuditStandardOptions() {
      return request({
        url: 'api/periodical/getAuditStandardOptions',
        method: 'get'
      })
    },
    getLevelOptions() {
      return request({
        url: 'api/periodical/getLevelOptions',
        method: 'get'
      })
    },
    getIncludedDatabaseOptions() {
      return request({
        url: 'api/periodical/getIncludedDatabaseOptions',
        method: 'get'
      })
    },
    getPublicationOptions() {
      return request({
        url: 'api/periodical/getPublicationOptions',
        method: 'get'
      })
    },
    getHistory(params) {
      return request({
        url: 'api/management/periodical/history',
        method: 'get',
        params
      })
    },
    updateDetail(data) {
      return request({
        url: 'api/management/periodical/updateDetail',
        method: 'post',
        data
      })
    },
    updatePrice(data) {
      return request({
        url: 'api/management/periodical/updatePrice',
        method: 'post',
        data
      })
    },
	updateStatus(data) {
	  return request({
	    url: 'api/management/periodical/updateStatus',
	    method: 'post',
	    data
	  })
	},
	updateSort(data) {
	  return request({
	    url: 'api/management/periodical/updateSort',
	    method: 'post',
	    data
	  })
	},
    updateHistory(data) {
      return request({
        url: 'api/management/periodical/updateHistory',
        method: 'post',
        data
      })
    },
    addHistory(data) {
      return request({
        url: 'api/management/periodical/addHistory',
        method: 'post',
        data
      })
    },
    deleteHistory(data) {
      return request({
        url: 'api/management/periodical/deleteHistory',
        method: 'post',
        data
      })
    },
    deletePeriodical(data) {
      return request({
        url: 'api/management/periodical/delete',
        method: 'post',
        data
      })
    },
    addPeriodical(data) {
      return request({
        url: 'api/management/periodical/add',
        method: 'post',
        data
      })
    }
  },
  customer: {
    getHistory(params) {
      return request({
        url: 'api/management/customer/list',
        method: 'get',
        params
      })
    },
    setAgent(data) {
      return request({
        url: 'api/management/customer/setAgent',
        method: 'post',
        data
      })
    },
    addUser(params) {
      return request({
        url: 'api/management/customer/addUser',
        method: 'post',
        params
      })
    },
    delete_user(params) {
      return request({
        url: 'api/management/customer/deleteUser',
        method: 'post',
        params
      })
    },
    updateInfo(data) {
      return request({
        url: 'api/management/customer/updateInfo',
        method: 'post',
        data
      })
    },
    setRole(data) {
      return request({
        url: 'api/management/customer/setRole',
        method: 'post',
        data
      })
    },
    getAgentOptions(params) {
      return request({
        url: 'api/management/customer/getAgentOptions',
        method: 'get',
        params
      })
    }
  },
  order: {
    getOrder(params) {
      return request({
        url: 'api/management/order/list',
        method: 'get',
        params
      })
    },
    read(data) {
      return request({
        url: 'api/management/order/read',
        method: 'post',
        data
      })
    }
  },
  article: {
    getArticle(params) {
      return request({
        url: 'api/management/article/list',
        method: 'get',
        params
      })
    },
    addArticle(data) {
      return request({
        url: 'api/management/article/add',
        method: 'post',
        data
      })
    },
    updateArticle(data) {
      return request({
        url: 'api/management/article/update',
        method: 'post',
        data
      })
    },
    deleteArticle(data) {
      return request({
        url: 'api/management/article/delete',
        method: 'post',
        data
      })
    },
    updateFile(data) {
      return request({
        url: 'api/management/article/updateFile',
        method: 'post',
        data
      })
    }
  },
  patent: {
    getPatent(params) {
      return request({
        url: 'api/management/patent/list',
        method: 'get',
        params
      })
    },
    addPatent(data) {
      return request({
        url: 'api/management/patent/add',
        method: 'post',
        data
      })
    },
    updatePatent(data) {
      return request({
        url: 'api/management/patent/update',
        method: 'post',
        data
      })
    },
    deletePatent(data) {
      return request({
        url: 'api/management/patent/delete',
        method: 'post',
        data
      })
    },
    updateFile(data) {
      return request({
        url: 'api/management/patent/updateFile',
        method: 'post',
        data
      })
    }
  },
  notice: {
    getNotice(params) {
      return request({
        url: 'api/management/notice/get',
        method: 'get',
        params
      })
    },
    updateNotice(data) {
      return request({
        url: 'api/management/notice/update',
        method: 'post',
        data
      })
    },
  },
  role: {
    getRole(params) {
      return request({
        url: 'api/management/role/list',
        method: 'get',
        params
      })
    },
    setRole(data) {
      return request({
        url: 'api/management/role/setRole',
        method: 'post',
        data
      })
    },
    updateInfo(data) {
      return request({
        url: 'api/management/role/updateInfo',
        method: 'post',
        data
      })
    },
	updateSort(data) {
	  return request({
	    url: 'api/management/role/updateSort',
	    method: 'post',
	    data
	  })
	},
    addUser(data) {
      return request({
        url: 'api/management/role/addUser',
        method: 'post',
        data
      })
    },
    getRoleOptions(params) {
      return request({
        url: 'api/management/role/getRoleOptions',
        method: 'get',
        params
      })
    }
  }
}

export default api
