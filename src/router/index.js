import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/periodical/periodical'
  },
  {
    path: '/periodical',
    component: Layout,
    meta: {
      title: '期刊列表',
      icon: 'el-icon-date'
    },
    children: [
      {
        path: 'periodical',
        name: 'periodical',
        component: () => import('@/views/periodical/index'),
        meta: { title: '期刊列表', icon: 'el-icon-date' }
      },
      {
        path: 'periodicalDetail',
        name: 'periodicalDetail',
        component: () => import('@/views/periodical/detail'),
        meta: { title: '期刊详情', icon: 'el-icon-plus' },
        hidden: true
      },
      {
        path: 'periodicalDetail2',
        name: 'periodicalDetail2',
        component: () => import('@/views/periodical/detail2'),
        meta: { title: '刊期', icon: 'el-icon-plus' },
        hidden: true
      },
    ]
  },
  {
    path: '/customer',
    component: Layout,
    meta: {
      title: '客户列表',
      icon: 'el-icon-user',
      roles: ['超级管理员']
    },
    children: [
      {
        path: 'customer',
        name: 'customer',
        component: () => import('@/views/customer/index'),
        meta: { title: '客户列表', icon: 'el-icon-user' }
      }
    ]
  }
]

export const asyncRoutes = [
  {
    path: '/role',
    component: Layout,
    meta: {
      title: '员工管理',
      icon: 'el-icon-wallet',
      roles: ['超级管理员']

    },
    children: [
      {
        path: 'role',
        name: 'role',
        component: () => import('@/views/role/index'),
        meta: { title: '员工管理', icon: 'el-icon-plus' }
      }
    ]
  },
  {
    path: '/role3',
    component: Layout,
    meta: {
      title: '代理管理',
      icon: 'el-icon-wallet',
      roles: ['超级管理员']

    },
    children: [
      {
        path: 'role3',
        name: 'role3',
        component: () => import('@/views/role/index'),
        meta: { title: '代理管理', icon: 'el-icon-plus' }
      }
    ]
  },
  {
    path: '/role2',
    component: Layout,
    meta: {
      title: '组稿老师',
      icon: 'el-icon-wallet',
      roles: ['超级管理员']

    },
    children: [
      {
        path: 'role2',
        name: 'role2',
        component: () => import('@/views/role/index'),
        meta: { title: '组稿老师', icon: 'el-icon-plus' }
      }
    ]
  },
  {
    path: '/order',
    component: Layout,
    meta: {
      title: '问题反馈',
      icon: 'el-icon-service',
      roles: ['管理员', '超级管理员']
    },
    children: [
      {
        path: 'order',
        name: 'order',
        component: () => import('@/views/order/index'),
        meta: { title: '反馈列表', icon: 'el-icon-service' }
      }
    ]
  },
  {
    path: '/article',
    component: Layout,
    meta: {
      title: '文章列表',
      icon: 'el-icon-document',
      roles: ['管理员', '超级管理员']
    },
    children: [
      {
        path: 'order',
        name: 'order',
        component: () => import('@/views/article/index'),
        meta: { title: '文章列表', icon: 'el-icon-document' }
      }
    ]
  },
  {
    path: '/patent',
    component: Layout,
    meta: {
      title: '专利专著',
      icon: 'el-icon-document',
      roles: ['管理员', '超级管理员']
    },
    children: [
      {
        path: '/patent',
        name: '专利专著',
        component: () => import('@/views/patent/index'),
        meta: { title: '专利专著', icon: 'el-icon-picture' }
      }
    ]
  },
  {
    path: '/notice',
    component: Layout,
    meta: {
      title: '其他',
      icon: 'el-icon-chat-dot-round',
      roles: ['管理员', '超级管理员']
    },
    children: [
      {
        path: '/notice',
        name: '其他',
        component: () => import('@/views/notice/index'),
        meta: { title: '其他', icon: 'el-icon-chat-dot-round' }
      }
    ]
  },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
